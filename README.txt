-- SUMMARY --
This module provides Pimple Container for Drupal projects.
Pimple is a simple Dependency Injection Container for PHP 5.3.

You can check details about it at: http://pimple.sensiolabs.org/

This module depends on Libraries API module.

-- INSTALLATION --
Install as usual.
